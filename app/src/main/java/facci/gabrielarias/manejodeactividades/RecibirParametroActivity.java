package facci.gabrielarias.manejodeactividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class RecibirParametroActivity extends AppCompatActivity {

    TextView lblParametro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_parametro);

        lblParametro = (TextView) findViewById(R.id.textViewParametro);


        String parametro = getIntent().getStringExtra("dato");
        lblParametro.setText(parametro);
    }
}
