package facci.gabrielarias.manejodeactividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PasarParametroActivity extends AppCompatActivity {

    private Button btnPasarParametro;
    private EditText txtParametro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);

        txtParametro = (EditText) findViewById(R.id.txtParametro);
        btnPasarParametro = (Button) findViewById(R.id.btnPasarParametro);

        btnPasarParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PasarParametroActivity.this, RecibirParametroActivity.class);
                String parametro = txtParametro.getText().toString();
                i.putExtra("dato", parametro);
                startActivity(i);
            }
        });

    }
}
